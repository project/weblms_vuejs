<?php

declare(strict_types = 1);

namespace Drupal\weblms_vuejs\Exception;

use Exception;

/**
 * Class LibraryException is the parent of all exceptions in the Vue libraries.
 */
class LibraryException extends Exception {

}
